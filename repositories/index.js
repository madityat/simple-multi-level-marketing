//import database
var db = require('../library/database');

class Index {

    async home (payload) {
        let result = {
            error: null,
            success: null,
            memberList: [],
            bonusLevel1: 0.0,
            bonusLevel2: 0.0,
            bonusTotal: 0.0,
            calculateMemberBonus: null,
            memberTreeStructure: {},
            memberTreeValue: {},
            ...payload
        }
        const memberList = await this.getMembers();
        if (memberList.error) {
            return { ...result, ...memberList };
        }
        result.memberList = memberList;

        const memberDownlineTree = await this.getMemberTree(payload.rootMember);
        if (memberDownlineTree.error) {
            return { ...result, ...memberDownlineTree };
        }
        result.memberTreeStructure = JSON.stringify(memberDownlineTree.memberTreeStructure);
        result.memberTreeValue = JSON.stringify(memberDownlineTree.memberTreeValue);

        if (payload.calculateMemberBonus) {
            const memberBonus = await this.getMemberBonus(payload.calculateMemberBonus);
            result = { ...result, ...memberBonus };
        }

        return result;
    }

    async getMembers(id = null) {
        try {
            const result = (id) ? await db.queryExecute(`SELECT * FROM t_users WHERE id = ${id}`) : await db.queryExecute('SELECT * FROM t_users');

            return result.error ? result : result.data;
        } catch (error) {
            return { error: error.message || error.error };
        }
    }

    async getMemberDownline(parentId, endLevel = 10, startLevel = 1) {
        try {
            let result = [];
            if (startLevel > endLevel) {
                return result;
            }
            const memberDownline = await db.queryExecute(`SELECT * FROM t_users WHERE parentId = ${parentId}`);
            for (let member of memberDownline.data) {
                result.push({ id: member.id, parentId: member.parentId, name: member.name, level: startLevel });
                const anotherDownline = await this.getMemberDownline(member.id, endLevel, startLevel+1);
                result = result.concat(anotherDownline.error ? [] : anotherDownline)
            };
            return result;
        } catch (error) {
            return { error: error.message || error.error };
        }
    }

    async getMemberTree(id = 0) {
        try {
            const result = {
                memberTreeStructure: {},
                memberTreeValue: {}
            };

            if (id == 0) {
                result.memberTreeValue = {
                    0: { trad: '<b>ADMIN</b>' }
                }
            } else {
                const rootMember = await this.getMembers(id);
                const memberBonus = await this.getMemberBonus(id);
                result.memberTreeValue[rootMember[0].id] = { trad: `<b>${rootMember[0].id} - ${rootMember[0].name}</b><br><small>Bonus : $${memberBonus.bonusTotal}</small>` };
            }
            const treeStructure = async (id) => {
                const result = {};
                const memberDownline = await this.getMemberDownline(id, 1, 1);
                for (let downline of memberDownline) {
                    result[downline.id] = await treeStructure(downline.id)
                }
                return Object.keys(result).length === 0 ? '' : result;
            }
            result.memberTreeStructure[id] = await treeStructure(id);

            const downlineList = await this.getMemberDownline(id);
            for (let downline of downlineList) {
                const memberBonus = await this.getMemberBonus(downline.id);
                result.memberTreeValue[downline.id] = { trad: `<b>${downline.id} - ${downline.name}</b><br><small>Bonus : $${memberBonus.bonusTotal}</small>` };
            }
            return result;
        } catch (error) {
            return { error: error.message || error.error };
        }
    }

    async getMemberBonus(id = 0) {
        try {
            const result = {
                bonusLevel1: 0.0,
                bonusLevel2: 0.0,
                bonusTotal: 0.0,
            }
            const levelOneDownline = await db.queryExecute(`SELECT * FROM t_users WHERE parentId = ${id}`);
            result.bonusLevel1 = levelOneDownline.data.length * 1.0;
            const levelTwoDownline = await db.queryExecute(`SELECT * FROM t_users WHERE parentId IN (SELECT id FROM t_users WHERE parentId = ${id})`);
            result.bonusLevel2 = levelTwoDownline.data.length * 0.5;

            result.bonusTotal = result.bonusLevel1 + result.bonusLevel2;
            return result;
        } catch (error) {
            return { error: error.message || error.error };
        }
    }

    async createMember(payload) {
        try {
            const result = await db.queryExecute(`INSERT INTO t_users(parentId, name) VALUES(${payload.parentId || null}, '${payload.name}')`);
            return result.error ? result : { success: 'Successfully create a member!' };
        } catch (error) {
            return { error: error.message || error.error };
        }
    }

    async changeMemberParent(payload) {
        try {
            if (payload.parentId != 0) {
                if (payload.parentId === payload.memberId) {
                    return { error: 'New Parent ID cannot be same with Choosen Member ID!' }
                }
                const memberDownline = await this.getMemberDownline(payload.memberId);
                if (memberDownline.find(downline => downline.id == payload.parentId)) {
                    return { error: 'New Parent ID cannot be same with Choosen Member Downline ID!' }
                }
            }
            const result = await db.queryExecute(`UPDATE t_users SET parentId = ${payload.parentId} WHERE id = ${payload.memberId}`);
            return result.error ? result : { success: 'Successfully change a member parent!' };
        } catch (error) {
            return { error: error.message || error.error };
        }
    }

}

module.exports = Index;