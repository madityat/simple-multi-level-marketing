const mysql = require('mysql');
 
const connection = mysql.createConnection({
   host: process.env.MYSQL_HOST,
   user: process.env.MYSQL_USERNAME,
   password: process.env.MYSQL_PASSWORD,
   database: process.env.MYSQL_DATABASE
 });

connection.connect(function(error){
   if(!!error){
     console.log(error);
   }else{
     console.log('MySQL Database Connection Created!');
   }
 })

 const queryExecute = (query) => {
  return new Promise((resolve, reject) => {
      connection.query(query, (err, rows) => {
        if (err) {
          reject({ error: err.message });
        } else {
          resolve({ data: rows });
        }
      });
  });
}
module.exports = {
  connection,
  queryExecute
}; 