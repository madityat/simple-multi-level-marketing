var express = require('express');
var router = express.Router();
const Index = require('../repositories');

/* GET home. */
router.get('/', async (req, res, next) => {
  const index = new Index();
  res.render('index', await index.home(req.query));
});

/* create member. */
router.post('/create', async (req, res, next) => {
  const index = new Index();
  const result = await index.createMember(req.body);
  res.redirect(result.error ? `/?error=${result.error}` : `/?success=${result.success}`);
});

/* change member parent. */
router.post('/change', async (req, res, next) => {
  const index = new Index();
  const result = await index.changeMemberParent(req.body);
  res.redirect(result.error ? `/?error=${result.error}` : `/?success=${result.success}`);
});

module.exports = router;
