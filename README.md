# simple-multi-level-marketing
### Prerequisites
Install this things before use this service
```
Node.js (v16.17.0 or latest)
Node Package Manager  (NPM)
SQL Datbase (MySql / MariaDB / etc)
```

### Installing
- Import the database & table from this file
```
db_simple_mlm.sql
```
- Create ENV file (.env) from this file and fill the value:
```
.env.example
```
- Then run this command to install all dependencies
```
npm install
```
- After the installation success, run this command
```
npm start
```
